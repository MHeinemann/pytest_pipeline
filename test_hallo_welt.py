import unittest as ui
from main import HalloWelt

class HalloWeltTestCase(ui.TestCase):

    def setUp(self):
        self.hw = HalloWelt()

    def test_passes_hallo_welt(self):
        self.assertEqual('Hallo Welt!', self.hw.show_message())
        self.hw.set_message("Moin moin")
        self.assertEqual('Moin moin', self.hw.show_message())

    def test_substract_numbers(self):
        self.assertEqual(1, self.hw.substract(3,2))

if __name__=='__main__':
    ui.main()