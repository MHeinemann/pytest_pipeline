"""Kleines Testmodule für Test-Pipelines."""
__author__ = 'michael heinemann'


class HalloWelt:
    """Hallo Welt Klasse"""

    def __init__(self):
        self.message = 'Hallo Welt!'

    def substract(self, i, j):
        """Substrahiert eine Zahl von einer anderen Zahl"""
        return i-j

    def set_message(self, message):
        """Aendert den Ausgangstext."""
        self.message = message

    def show_message(self):
        """Zeigen des Textes."""
        return self.message


if __name__=='__main__':
    myGreeting = HalloWelt()
    print(myGreeting.show_message())